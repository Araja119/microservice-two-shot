from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoe, BinVO
from django.http import JsonResponse, HttpResponse

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
        "bin_number",
        "bin_size",
    ]


class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bins",
        "id",

    ]
    encoders = {"bins": BinVOEncoder()}



@require_http_methods(["GET", "POST","DELETE"])
def api_shoes(request):

    if request.method == "GET":
        shoe = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoe},
            encoder=ShoeEncoder,
        )

    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            href = f"/api/bins/{content['bins']}/"
            bins = BinVO.objects.get(import_href=href)
            content["bins"] = bins
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        content = json.loads(request.body)
        print(content)
        id = content["id"]
        try:
            shoe = Shoe.objects.get(id=id)
            shoe.delete()
            return HttpResponse(status=204)
        except Shoe.DoesNotExist:
                return JsonResponse({"message": "Shoe not found"}, status=404)
