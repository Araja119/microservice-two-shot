from django.shortcuts import render
from .models import Hats, LocationVO
import json
from django.http import JsonResponse, HttpResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder



class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]

class HatEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id",
    ]
    encoders = {"location": LocationVOEncoder()}


@require_http_methods(["GET", "POST", "DELETE"])
def api_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            location_href = f"/api/locations/{content['location']}/"
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message:" "Invalid Location"},
                status=400,
            )
        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        content = json.loads(request.body)
        print(content)
        id = content["id"]
        try:
            hat = Hats.objects.get(id=id)
            hat.delete()
            return HttpResponse(status=204)
        except Hats.DoesNotExist:
                return JsonResponse({"message": "Hat not found"}, status=404)
