# Wardrobify

Team:

* Abdullah Raja - Hats
* Brittany Welborn - Shoes

## Design



## How to Run this App


## Diagram
 - Put diagram here



## API Documentation

### URLs and Ports
 - Put URLs and ports for services here

### Shoe API
 - Put Shoe API documentation here

### Hats API
 - Put Hats API documentation here

## Value Objects
 - Identification of value objects for each service goes here
Shared in


## Shoes microservice

- Manufacturer
- Model Name
- Color
- Picture URL
- Location in Wardrobe (bin)

## Hats microservice


Models:
 - Fabric
 - Style Name
 - Color
 - Picture URL
 - Location in Wardrobe

## Getting Started

**Make sure you have Docker, Git, and Node.js 18.2 or above**

1. Fork this repository

2. Clone the forked repository onto your local computer:
git clone <<https://gitlab.com/Araja119/microservice-two-shot>>

3. Build and run the project using Docker with these commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```

Wardrobify is made up of 3 microservices which interact with one another.

- **Hats**
- **Shoes**
- **Wardrobe**

## Integration - How we put the "team" in "team"

Our Shoes and Hats domains work together with our Wardrobe domain to make everything here at **Wardrobify** possible.

This starts at our wardrobe domain, which keeps the information of both our shoes and hats, storing their locations within their respective closets/shelves/sections.
We utilize our poller to get the *bin* or *location* data for our microservices. The poller's job is to send a GET request to the wardrobe API to receive a list of locations.

Based on each location, it updates or creates a record in our hats/shoes microservices. This process is there to make sure that our microservice has data syncronized with our wardrobe domain in real-time.

## Accessing Endpoints to Send and View Data: Access Through Insomnia & Your Browser

### Wardrobe:
| Action | Method | URL
| Hats Port: 8090 | Shoes Port: 8080
| ----------- | ----------- | ----------- |
| List location | GET | http://localhost:8100/api/location/
| Create a location | POST | http://localhost:8100/api/location/ |
| Get a specific location | GET | http://localhost:8100/api/location/id/
| Delete a specific location | DELETE | http://localhost:8100/api/location/id


## Diagram
![Image](diagram.png)

## API Documentation
API used:
http://localhost:8100/api/locations/


### URLs and Ports
 - Put URLs and ports for services here
 http://localhost:3000/
 http://localhost:3000/hats
 http://localhost:3000/shoes
 http://localhost:3000/hats/new
 http://localhost:3000/shoes/new

### Shoe API
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8080/api/shoes/
| Create an automobile | POST | http://localhost:8080/api/shoes/
| Delete a specific automobile | DELETE | http://localhost:8080/api/shoes/id/

### Hats API
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8090/api/hats/
| Create an automobile | POST | http://localhost:8090/api/hats/
| Delete a specific automobile | DELETE | http://localhost:8090/api/hats/id/

## Value Objects
 - Identification of value objects for each service goes here
| Value objects for the Hats Microservice:
    id
    closet_name
    section_number
    shelf_number

| Value objects for the Shoes Microservice:
    id
    closet_name
    bin_number
    bin_size
