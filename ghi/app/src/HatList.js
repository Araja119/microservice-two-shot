import { useState, useEffect} from 'react';

function HatList() {
    const [hats, setHats] = useState([])


    const getData = async ()=> {
        // We are going into our hats database and extracting its data
        const response = await fetch('http://localhost:8090/api/hats')
        if (response.ok) {
            // Turn JSON into an array
            const { hats } = await response.json()
            setHats(hats)
        } else {
            console.error("An error occurred fetching the data")
        }
    }

    useEffect(()=> {
        getData()
        }, []);


    const deleteHat = async (id) => {
      console.log("You will delete hat", id)
      const fetchConfig = {
        method: 'DELETE',
        body: JSON.stringify({id: id}),
      }
      const response = await fetch('http://localhost:8090/api/hats', fetchConfig)
      if (response.ok) {
        setHats(hats.filter(hat => hat.id !== id))
        console.log("Hat was deleted")
      }
      else {
        console.log("Hat was not deleted")
      }
    }

return (
// Put a set of table headers at the top of the page
<table className="table table-striped">
  <thead>
    <tr>
      <th scope="col">Fabric</th>
      <th scope="col">Style Name</th>
      <th scope="col">Color</th>
      <th scope="col">Picture URL</th>
      <th scope="col">Delete</th>
    </tr>
  </thead>

 {/*Under the table headers, put in all of the data from our database under each header  */}
  <tbody>
      {/* For every hat in hats... do this: */}
        {hats.map(hat => {
          return (
            <tr key={hat.id}>
              <td>{ hat.fabric}</td>
              <td>{ hat.style_name }</td>
              <td>{ hat.color }</td>
              <td> <img src={hat.picture_url} width={50}/></td>
              <td><button onClick={() => deleteHat(hat.id)}>Delete</button></td>
            </tr>
          );
        })}
        </tbody>
      </table>
)
}


export default HatList;
