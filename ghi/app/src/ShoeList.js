import React, {useState, useEffect} from 'react';

function ShoeList() {
    const [shoes, setShoes] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/shoes')
        if (response.ok) {
            const { shoes } = await response.json()
            setShoes(shoes)
        } else {
            console.error("An error occurred fetching the data")
        }
    }

    useEffect(() => {
        getData()
    }, []);

    const deleteShoe = async (id) => {
      console.log("You will delete shoe", id)
      const fetchConfig = {
        method: 'DELETE',
        body: JSON.stringify({id: id}),
      }
      const response = await fetch('http://localhost:8080/api/shoes', fetchConfig)
      if (response.ok) {
        setShoes(shoes.filter(shoe => shoe.id !== id))
        console.log("Shoe was deleted")
      }
      else {
        console.log("Shoe was not deleted")
      }
    }

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th scope="col">Manufacturer</th>
          <th scope="col">Model Name</th>
          <th scope="col">Color</th>
          <th scope="col">Picture URL</th>
          <th scope="col">Wardrobe Bin</th>
        </tr>
      </thead>
      <tbody>
        {shoes.map(shoe => {
          return (
          <tr key={shoe.model_name}>
            <td>{shoe.manufacturer}</td>
            <td>{shoe.model_name}</td>
            <td>{shoe.color}</td>
            <td> <img src={shoe.picture_url} width={50}/></td>
            <td><button onClick={() => deleteShoe(shoe.id)}>Delete</button></td>
            {/* <td>{shoe.bins}</td> */}
          </tr>
          )
        })}
      </tbody>
    </table>
  );
}

export default ShoeList;
