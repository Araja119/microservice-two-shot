import React, { useEffect, useState } from 'react';

function HatForm() {
    const wardrobeUrl = "http://localhost:8100/api/locations/";
    const hatsUrl = "http://localhost:8090/api/hats/";

    const [formData, setFormData] = useState({
        fabric: '',
        style_name: '',
        color: '',
        picture_url: '',
        location: '',
    });

    const [locations, setLocations] = useState([]);

    const getData = async () => {
        const response = await fetch(wardrobeUrl);
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setLocations(data.locations); // Assuming the response directly contains the locations array
        } else {
            console.error('An error occurred fetching the data');
        }
    };

    useEffect(() => {
        getData();
    }, []);

    const handleFormChange = (e) => {
        const { name, value } = e.target;
        // '...' = spread operator
        setFormData({ ...formData, [name]: value });
    };

    // When you do anything other than GET, you need to declare a fetch configuration which lays out instructions for the
    // type of request you want to send.
    const handleSubmit = async (event) => {
        event.preventDefault();
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatsUrl, fetchConfig);
        if (response.ok) {
            console.log("Hat created successfully!");
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={event => handleSubmit(event)} id="create-hat-form">
                    <div className="form-floating mb-3">
                            <input onChange={event => handleFormChange(event)} value={formData.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={event => handleFormChange(event)} value={formData.style_name} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control" />
                            <label htmlFor="style_name">Style Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={event => handleFormChange(event)} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="picture_url">Picture URL</label>
                            <textarea onChange={event => handleFormChange(event)} value={formData.picture_url} id="picture_url" rows="3" name="picture_url" className="form-control"></textarea>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="location">Location</label>
                            <select onChange={event => handleFormChange(event)} value={formData.location} required className="form-select" name="location" id="location">
                                <option value="">Select a location</option>
                                {locations.map((location, index) => (
                                    <option key={index} value={location.id}>{location.closet_name}</option>
                                ))}
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default HatForm;
