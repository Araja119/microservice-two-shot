import React, { useEffect, useState } from 'react';
function ShoeForm() {
    const wardrobeUrl = "http://localhost:8100/api/bins/";
    const shoesUrl = "http://localhost:8080/api/shoes/";
    // State to hold form data
    const [formData, setFormData] = useState({
        manufacturer: '',
        model_name: '',
        color: '',
        picture_url: '',
        bins: '',
    });
    const [bins, setBins] = useState([])
    // Function to handle form field changes
    const handleFormChange = (e) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };
    // Function to handle form submission
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            manufacturer: formData.manufacturer,
            model_name: formData.model_name,
            color: formData.color,
            picture_url: formData.picture_url,
            bins: formData.bins,
        }

    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
        };
        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok) {
        const newBins = await response.json();
    };
        // Logic to handle form submission, e.g., POST request
        console.log(formData); // For debugging
    };
    // Function to fetch initial data
    const getData = async () => {
        const response = await fetch(wardrobeUrl);
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setBins(data.bins); // Assuming the response directly contains the locations array
        } else {
            console.error('An error occurred fetching the data');
        }
    };
    useEffect(() => {
        getData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new shoe</h1>
                    <form onSubmit={event => handleSubmit(event)} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input onChange={event => handleFormChange(event)} value={formData.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={event => handleFormChange(event)} value={formData.model} placeholder="Model_Name" required type="text" name="model_name" id="model_name" className="form-control" />
                            <label htmlFor="model_name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={event => handleFormChange(event)} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="picture_url">Picture URL</label>
                            <textarea onChange={event => handleFormChange(event)} value={formData.picture_url} id="picture_url" rows="3" name="picture_url" className="form-control"></textarea>
                        </div>
                        <div className="mb-3">
                            <select onChange={event => handleFormChange(event)} value={formData.bins} required className="form-select" name="bins" id="bins">
                                <option value="">Please select a bin</option>
                                {bins.map((bin, index) => (
                                    <option key={index} value={bin.id}>{bin.closet_name}</option>
                                ))}
                                {/* Add more options here as needed */}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default ShoeForm;
